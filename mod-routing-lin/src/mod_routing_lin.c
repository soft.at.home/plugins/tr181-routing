/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <stdio.h>

#include "mod_routing_lin.h"
#include "ipvx_route_handler.h"

#define ME "mod-routing"

typedef struct {
    amxm_shared_object_t* so;
} _mod_routing_lin_t;

static _mod_routing_lin_t lin;

static int lin_add_route(UNUSED const char* function_name,
                         amxc_var_t* args,
                         amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int ret_val = -1;
    const char* origin = GETP_CHAR(args, "Origin");

    when_str_empty_trace(origin, exit, ERROR, "No origin provided")
    if((strcmp(origin, "IPCP") == 0) || (strcmp(origin, "3GPP-NAS") == 0)) {
        ret_val = nl_route_add(args);
    } else {
        ret_val = ipvx_route_add(args);
    }

exit:
    amxc_var_set(int32_t, ret, ret_val);
    SAH_TRACEZ_OUT(ME);
    return 0;
}

static int lin_remove_route(UNUSED const char* function_name,
                            amxc_var_t* args,
                            amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int ret_val = -1;
    const char* origin = GETP_CHAR(args, "Origin");

    when_str_empty_trace(origin, exit, ERROR, "No origin provided")
    if((strcmp(origin, "IPCP") == 0) || (strcmp(origin, "3GPP-NAS") == 0)) {
        ret_val = nl_route_del(args);
    } else {
        ret_val = ipvx_route_remove(args);
    }

exit:
    amxc_var_set(int32_t, ret, ret_val);
    SAH_TRACEZ_OUT(ME);
    return 0;
}

static AMXM_CONSTRUCTOR routing_lin_start(void) {
    SAH_TRACEZ_IN(ME);
    amxm_module_t* mod = NULL;
    lin.so = amxm_so_get_current();

    amxm_module_register(&mod, lin.so, MOD_ROUTING_CTRL);
    amxm_module_add_function(mod, "add-route", lin_add_route);
    amxm_module_add_function(mod, "remove-route", lin_remove_route);

    SAH_TRACEZ_OUT(ME);
    return 0;
}

static AMXM_DESTRUCTOR routing_lin_stop(void) {
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_OUT(ME);
    return 0;
}
