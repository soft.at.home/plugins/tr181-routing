# TR181 compatible Routing manager

## Introduction

This is an Ambiorix plug-in for a TR-181 compatible Routing manager.

## Building

### Prerequisites

- [libamxc](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxc)
- [libamxp](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxp)
- [libamxj](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxj)
- [libamxd](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxd)
- [libamxb](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxb)
- [libamxo](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxo)
- [libamxm](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxm)
- [libsahtrace](https://gitlab.com/soft.at.home/network/libsahtrace)
- [libnetmodel](https://gitlab.com/prpl-foundation/components/core/libraries/libnetmodel)
- [libdhcpoptions](https://gitlab.com/prpl-foundation/components/core/libraries/libdhcpoptions)

You can install these libraries from source or using their debian packages. To install them from source, refer to their corresponding repositories for more information.
To install them using debian packages, you can run

```bash
sudo apt update
sudo apt install sah-lib-sahtrace-dev libamxc libamxp libamxj libamxd libamxb libamxo libamxm libnetmodel libdhcpoptions
```

### Build and install tr181-routing

1. Clone the git repository

    To be able to build it, you need the source code. So open the desired target directory and clone this plug-in in it.

    ```bash
    mkdir ~/workspace/amx/plugins
    cd ~/workspace/amx/plugins
    git clone git@gitlab.com:prpl-foundation/components/core/plugins/tr181-routing.git
    ```

1. Build it

    When using the internal gitlab, you must define an environment variable `VERSION_PREFIX` before building.

    ```bash
    export VERSION_PREFIX="master_"
    ```

    After the variable is set, you can build the plug-in.

    ```bash
    cd ~/workspace/amx/plugins/tr181-routing
    make
    ```

1. Install it

    You can use the install target in the makefile to install the plug-in

    ```bash
    cd ~/workspace/amx/plugins/tr181-routing
    sudo -E make install
    ```

### Running the plug-in

During installation a symbolic link is created to amxrt:

```text
/usr/bin/tr181-routing -> /usr/bin/amxrt
```

This allows you to run the Routing manager using the `tr181-routing` command. `amxrt` will find the relevant odl files in `/etc/amx/tr181-routing`. In the current configuration (see `odl/routing-manager.odl`) the file `/etc/amx/routing-manager/routing-manager-defaults.odl` is loaded on startup of the routing manager plug-in. You can edit this file to change/add `Routing.Router.` instances.
