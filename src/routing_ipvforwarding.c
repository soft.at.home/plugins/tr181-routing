/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "routing_ipvforwarding.h"
#include "routing_ipvforwarding_info.h"
#include "netmodel/client.h"

#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <arpa/inet.h>

/**********************************************************
* MACRO definitions
**********************************************************/

#define string_empty(x) ((x == NULL) || (*x == '\0'))

#define IPV4FORWARDING_ORIGIN_DHCPV4 "DHCPv4"
#define IPV4FORWARDING_ORIGIN_IPCP "IPCP"
#define IPV4FORWARDING_ORIGIN_RIP "RIP"

#define IPV6FORWARDING_ORIGIN_DHCPV6 "DHCPv6"
#define IPV6FORWARDING_ORIGIN_RA "RA"
#define IPV6FORWARDING_ORIGIN_RIPNG "RIPng"

#define IPVFORWARDING_ORIGIN_OSPF "OSPF"
#define IPV4FORWARDING_ORIGIN_3GPP_NAS "3GPP-NAS"
#define IPVFORWARDING_ORIGIN_STATIC "Static"
#define IPVFORWARDING_ORIGIN_AUTOMATIC "Automatic"

/**********************************************************
* Type definitions
**********************************************************/

/**********************************************************
* Variable declarations
**********************************************************/

/**********************************************************
* Function Prototypes
**********************************************************/
static int is_ipv4_forwarding(amxd_object_t* obj);
static amxd_status_t routing_ipvforwarding_update_routing_table(route_forwarding_info_t* info, router_action_t action);
static amxd_status_t routing_ipvforwarding_handle_origins(route_forwarding_info_t* route_forwarding_info, const cstring_t origin);
static bool handle_dhcpv4_origin(route_forwarding_info_t* route_forwarding_info);
static bool handle_ipcp_origin(route_forwarding_info_t* route_forwarding_info);
static bool handle_3gppnas_origin(route_forwarding_info_t* route_forwarding_info);
static bool handle_static_origin(route_forwarding_info_t* route_forwarding_info);
static void routing_ipvforwarding_enable_query(amxd_object_t* obj);
static void update_is_static_route_param(amxd_object_t* instance);
static void ipvforwarding_set_usersetting_flag(amxd_object_t* instance);
static void ipvforwarding_clear_usersetting_flag(amxd_object_t* instance);
static void update_childs_enable_param(amxd_object_t* router_ipvX_forwarding_obj, bool parent_enabled);
static void routing_ipvforwarding_dhcp_defr_ip_changed_cb(const char* const sig_name,
                                                          const amxc_var_t* const data,
                                                          void* const priv);
static void routing_ipvforwarding_dhcp_ip_changed_cb(const char* const sig_name,
                                                     const amxc_var_t* const data,
                                                     void* const priv);
static void routing_ipvforwarding_intf_changed_cb(const char* const sig_name,
                                                  const amxc_var_t* const data,
                                                  void* const priv);
static void routing_ipvforwarding_name_changed_cb(const char* const sig_name,
                                                  const amxc_var_t* const data,
                                                  void* const priv);
static void routing_ipvforwarding_is_up_changed_cb(const char* const sig_name,
                                                   const amxc_var_t* const data,
                                                   void* const priv);
static void routing_ipvforwarding_ip_changed_cb(const char* const sig_name,
                                                const amxc_var_t* const data,
                                                void* const priv);
static void routing_ipvforwarding_static_addr_changed_cb(const char* const sig_name,
                                                         const amxc_var_t* const data,
                                                         void* const priv);
static amxd_status_t routing_ipvforwarding_set_gw_value(amxd_object_t* obj, const cstring_t default_route_gateway);
static bool routing_ipvforwarding_remove_unused_param_in_var(amxc_var_t* parameters);
static void routing_ipvforwarding_reset_gw_value(amxd_object_t* obj);
static void get_variant_for_routing_table(route_forwarding_info_t* info, amxc_var_t* data);
static void routing_ipvforwarding_set_status(amxd_object_t* obj, router_action_t status);

/**********************************************************
* Functions
**********************************************************/

static void routing_ipvforwarding_reset_gw_value(amxd_object_t* obj) {
    SAH_TRACEZ_IN(ME);
    cstring_t origin = NULL;
    amxd_status_t status = amxd_status_unknown_error;

    when_null_trace(obj, exit, ERROR, "No object given");

    origin = amxd_object_get_value(cstring_t, obj, "Origin", NULL);
    when_null_trace(origin, exit, ERROR, "Could not find the origin parameter");
    if((strcmp(origin, IPV4FORWARDING_ORIGIN_DHCPV4) == 0) || (strcmp(origin, IPV4FORWARDING_ORIGIN_IPCP) == 0) ||
       (strcmp(origin, IPV4FORWARDING_ORIGIN_3GPP_NAS) == 0)) {
        status = routing_ipvforwarding_set_gw_value(obj, NULL);
        when_failed_trace(status, exit, ERROR, "Failed to set the gw value to NULL");
    }
    status = amxd_status_ok;
exit:
    routing_ipvforwarding_set_status(obj, status == amxd_status_ok ? action_remove : action_error);
    free(origin);
    SAH_TRACEZ_OUT(ME);
}

static amxd_status_t routing_ipvforwarding_set_gw_value(amxd_object_t* obj, const cstring_t default_route_gateway) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    when_null_trace(obj, exit, ERROR, "no object found");

    SAH_TRACEZ_INFO(ME, "Setting GatewayIPAddress to '%s'", (default_route_gateway == NULL ? "" : default_route_gateway));
    amxd_trans_select_object(&trans, obj);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_value(cstring_t, &trans, "GatewayIPAddress", (default_route_gateway == NULL ? "" : default_route_gateway));
    status = amxd_trans_apply(&trans, routing_manager_get_dm());
    when_failed_trace(status, exit, ERROR, "Failed to apply the new value for the GatewayIPAddress param")

    SAH_TRACEZ_INFO(ME, "Successfully set the GatewayIPAddress to '%s'", (default_route_gateway == NULL ? "" : default_route_gateway));
exit:
    amxd_trans_clean(&trans);
    SAH_TRACEZ_OUT(ME);
    return status;
}

static cstring_t get_first_string_in_var(const amxc_var_t* const data) {
    SAH_TRACEZ_IN(ME);
    cstring_t first_string = NULL;
    amxc_var_t string_list_var;

    amxc_var_init(&string_list_var);

    when_null_trace(data, exit, ERROR, "Bad input parameter to split the string");

    amxc_var_convert(&string_list_var, data, AMXC_VAR_ID_CSV_STRING);
    amxc_var_cast(&string_list_var, AMXC_VAR_ID_LIST);
    first_string = amxc_var_dyncast(cstring_t, amxc_var_get_first(&string_list_var));
exit:
    amxc_var_clean(&string_list_var);
    SAH_TRACEZ_OUT(ME);
    return first_string;
}

amxd_status_t routing_ipvforwarding_disable_query(amxd_object_t* obj) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    cstring_t origin = NULL;
    route_forwarding_info_t* route_forwarding_info = NULL;

    when_null_trace(obj, exit, ERROR, "No object given");
    origin = amxd_object_get_value(cstring_t, obj, "Origin", NULL);
    route_forwarding_info = (route_forwarding_info_t*) obj->priv;
    when_null_trace(route_forwarding_info, exit, ERROR, "No route_forwarding_info_t found in the object");
    when_null_trace(origin, exit, ERROR, "No origin parameter found");
    when_false_trace(strcmp(origin, IPVFORWARDING_ORIGIN_AUTOMATIC), exit, NOTICE, "Cannot change IPvXForwarding object with the origin value 'Automatic'");

    status = routing_ipvforwarding_update_routing_table(route_forwarding_info, action_remove);
    when_failed_trace(status, exit, ERROR, "Failed to disable instance");
    SAH_TRACEZ_INFO(ME, "Successfully disabled an ipforwarding query");
exit:
    route_forwarding_info_clear_content((route_forwarding_info_t*) obj->priv);
    free(origin);
    SAH_TRACEZ_OUT(ME);
    return status;
}

static void routing_ipvforwarding_enable_query(amxd_object_t* obj) {
    SAH_TRACEZ_IN(ME);
    bool instance_enabled = false;
    bool parent_enabled = false;
    cstring_t origin = NULL;
    route_forwarding_info_t* route_forwarding_info = NULL;
    amxd_status_t status = amxd_status_unknown_error;

    when_null_trace(obj, exit, ERROR, "no object found");
    route_forwarding_info = (route_forwarding_info_t*) obj->priv;
    when_null_trace(route_forwarding_info, exit, ERROR, "no route_forwarding_info_t found in the object");
    when_null_trace(route_forwarding_info->obj, exit, ERROR, "Not able to find the default route info object");
    when_false_trace(route_forwarding_info->intf_path == NULL, exit, ERROR, "intf path should be empty");
    when_false_trace(route_forwarding_info->nm_query == NULL, exit, ERROR, "netmodel query should be empty");

    instance_enabled = amxd_object_get_value(bool, obj, "Enable", NULL);
    parent_enabled = amxd_object_get_value(bool, amxd_object_get_parent(amxd_object_get_parent(obj)), "Enable", NULL);
    if((parent_enabled == false) || (instance_enabled == false)) {
        goto exit;
    }

    origin = amxd_object_get_value(cstring_t, route_forwarding_info->obj, "Origin", NULL);
    when_null_trace(origin, exit, ERROR, "Could not get the origin parameter");
    if(strcmp(origin, IPVFORWARDING_ORIGIN_AUTOMATIC) == 0) {
        status = amxd_status_ok;
        routing_ipvforwarding_set_status(route_forwarding_info->obj, action_add);
        SAH_TRACEZ_NOTICE(ME, "Cannot change IPvXForwarding object with the origin value '%s'", IPVFORWARDING_ORIGIN_AUTOMATIC);
        goto exit;
    }

    route_forwarding_info->intf_path = amxd_object_get_value(cstring_t, route_forwarding_info->obj, "Interface", NULL);
    when_null_trace(route_forwarding_info->intf_path, exit, ERROR, "the Interface parameter could not be found");

    status = routing_ipvforwarding_handle_origins(route_forwarding_info, origin);
    update_is_static_route_param(obj);
exit:
    if(status != amxd_status_ok) {
        routing_ipvforwarding_set_status(route_forwarding_info->obj, action_error);
    }
    free(origin);
    SAH_TRACEZ_OUT(ME);
}

static amxd_status_t routing_ipvforwarding_handle_origins(route_forwarding_info_t* route_forwarding_info, const cstring_t origin) {
    SAH_TRACEZ_IN(ME);
    bool success = false;
    amxd_status_t status = amxd_status_unknown_error;

    route_forwarding_info->is_up = true;
    if(strcmp(origin, IPV4FORWARDING_ORIGIN_DHCPV4) == 0) {
        success = handle_dhcpv4_origin(route_forwarding_info);
    } else if(strcmp(origin, IPV4FORWARDING_ORIGIN_IPCP) == 0) {
        success = handle_ipcp_origin(route_forwarding_info);
    } else if(strcmp(origin, IPV4FORWARDING_ORIGIN_3GPP_NAS) == 0) {
        success = handle_3gppnas_origin(route_forwarding_info);
    } else if(strcmp(origin, IPVFORWARDING_ORIGIN_STATIC) == 0) {
        if(*(route_forwarding_info->intf_path) == 0) {//it is possible to add a route without an intf_path
            amxc_var_t var;
            amxc_var_init(&var);
            routing_ipvforwarding_intf_changed_cb(NULL, &var, (void*) route_forwarding_info);
            amxc_var_clean(&var);
            success = true;
        } else {
            success = handle_static_origin(route_forwarding_info);
        }
    } else {
        SAH_TRACEZ_ERROR(ME, "Invalid origin specified");
    }

    if(!success) {
        SAH_TRACEZ_ERROR(ME, "Failed to add a query to netmodel, make sure the interface param has a valid value");
        free(route_forwarding_info->intf_path);
        route_forwarding_info->intf_path = NULL;
        free(route_forwarding_info->intf_name);
        route_forwarding_info->intf_name = NULL;
    } else {
        status = amxd_status_ok;
        SAH_TRACEZ_INFO(ME, "Successfully added a query to netmodel, for the default route intf %s", route_forwarding_info->intf_path);
    }
    SAH_TRACEZ_OUT(ME);
    return status;
}

static bool handle_dhcpv4_origin(route_forwarding_info_t* route_forwarding_info) {
    SAH_TRACEZ_IN(ME);
    bool success = false;
    route_forwarding_info->nm_query = netmodel_openQuery_getFirstParameter(route_forwarding_info->intf_path, "routing-manager", "IPRouters", "ipv4-up", netmodel_traverse_down, routing_ipvforwarding_dhcp_ip_changed_cb, (void*) route_forwarding_info);
    route_forwarding_info->nm_query_ipv4_addr = netmodel_openQuery_getAddrs(route_forwarding_info->intf_path, "routing-manager", "ipv4", netmodel_traverse_down, routing_ipvforwarding_dhcp_defr_ip_changed_cb, (void*) route_forwarding_info);
    success = (route_forwarding_info->nm_query != NULL) && (route_forwarding_info->nm_query_ipv4_addr != NULL);
    SAH_TRACEZ_OUT(ME);
    return success;
}

static bool handle_ipcp_origin(route_forwarding_info_t* route_forwarding_info) {
    SAH_TRACEZ_IN(ME);
    bool success = false;
    route_forwarding_info->is_up = false;
    route_forwarding_info->nm_query_is_up = netmodel_openQuery_hasFlag(route_forwarding_info->intf_path,
                                                                       "routing-manager",
                                                                       "ipv4-up",
                                                                       NULL,
                                                                       netmodel_traverse_this,
                                                                       routing_ipvforwarding_is_up_changed_cb,
                                                                       (void*) route_forwarding_info);
    route_forwarding_info->nm_query = netmodel_openQuery_getFirstParameter(route_forwarding_info->intf_path,
                                                                           "routing-manager",
                                                                           "NetDevName",
                                                                           "ppp-up",
                                                                           netmodel_traverse_down,
                                                                           routing_ipvforwarding_name_changed_cb,
                                                                           (void*) route_forwarding_info);
    route_forwarding_info->nm_query2 = netmodel_openQuery_getFirstParameter(route_forwarding_info->intf_path,
                                                                            "routing-manager",
                                                                            "RemoteIPAddress",
                                                                            "ppp-up",
                                                                            netmodel_traverse_down,
                                                                            routing_ipvforwarding_ip_changed_cb,
                                                                            (void*) route_forwarding_info);
    success = (route_forwarding_info->nm_query_is_up != NULL) && (route_forwarding_info->nm_query != NULL) &&
        (route_forwarding_info->nm_query2 != NULL);
    SAH_TRACEZ_OUT(ME);
    return success;
}

static bool handle_3gppnas_origin(route_forwarding_info_t* route_forwarding_info) {
    SAH_TRACEZ_IN(ME);
    bool success = false;
    route_forwarding_info->is_up = false;

    SAH_TRACEZ_INFO(ME, "intf_path:%s intf_name:%s ipv4_route_address:%s", route_forwarding_info->intf_path, route_forwarding_info->intf_name, route_forwarding_info->ipv4_route_address);
    route_forwarding_info->nm_query_is_up = netmodel_openQuery_hasFlag(route_forwarding_info->intf_path,
                                                                       "routing-manager",
                                                                       "ipv4-up && netdev-up",
                                                                       NULL,
                                                                       netmodel_traverse_down,
                                                                       routing_ipvforwarding_is_up_changed_cb,
                                                                       (void*) route_forwarding_info);
    route_forwarding_info->nm_query = netmodel_openQuery_getFirstParameter(route_forwarding_info->intf_path,
                                                                           "routing-manager",
                                                                           "NetDevName",
                                                                           "cellular-up",
                                                                           netmodel_traverse_down,
                                                                           routing_ipvforwarding_name_changed_cb,
                                                                           (void*) route_forwarding_info);
    route_forwarding_info->nm_query2 = netmodel_openQuery_getFirstParameter(route_forwarding_info->intf_path,
                                                                            "routing-manager",
                                                                            "IPAddress",
                                                                            "cellular-up",
                                                                            netmodel_traverse_down,
                                                                            routing_ipvforwarding_ip_changed_cb,
                                                                            (void*) route_forwarding_info);
    success = (route_forwarding_info->nm_query_is_up != NULL) && (route_forwarding_info->nm_query != NULL) &&
        (route_forwarding_info->nm_query2 != NULL);
    SAH_TRACEZ_OUT(ME);
    return success;
}

static bool handle_static_origin(route_forwarding_info_t* route_forwarding_info) {
    SAH_TRACEZ_IN(ME);
    const char* flag = NULL;
    bool success = false;

    if(is_ipv4_forwarding(route_forwarding_info->obj) == 1) {
        flag = "ipv4";
    } else if(is_ipv4_forwarding(route_forwarding_info->obj) == 0) {
        flag = "ipv6";
    } else {
        SAH_TRACEZ_ERROR(ME, "Couldn't determine if the object is an IPv4 or IPv6 forwarding instance");
        goto exit;
    }
    route_forwarding_info->nm_query = netmodel_openQuery_getAddrs(route_forwarding_info->intf_path,
                                                                  "routing-manager",
                                                                  flag,
                                                                  netmodel_traverse_down,
                                                                  routing_ipvforwarding_static_addr_changed_cb,
                                                                  (void*) route_forwarding_info);
    success = (route_forwarding_info->nm_query != NULL);
exit:
    SAH_TRACEZ_OUT(ME);
    return success;
}

static void routing_ipvforwarding_intf_changed_cb(UNUSED const char* const sig_name,
                                                  const amxc_var_t* const data,
                                                  void* const priv) {
    SAH_TRACEZ_IN(ME);
    route_forwarding_info_t* route_forwarding_info = (route_forwarding_info_t*) priv;
    amxd_status_t status = amxd_status_unknown_error;

    when_null_trace(route_forwarding_info, exit, ERROR, "no route_forwarding_info_t found in the object");
    when_null_trace(data, exit, ERROR, "Invalid data parameter");
    when_null_trace(route_forwarding_info->intf_path, exit, ERROR, "the Interface parameter should not be NULL");

    free(route_forwarding_info->intf_name);
    route_forwarding_info->intf_name = amxc_var_dyncast(cstring_t, data);

    status = routing_ipvforwarding_update_routing_table(route_forwarding_info, action_add);
    when_failed_trace(status, exit, ERROR, "Failed to update the routing table");
exit:
    routing_ipvforwarding_set_status(route_forwarding_info->obj, status == amxd_status_ok ? action_add : action_error);
    SAH_TRACEZ_OUT(ME);
}

static amxd_status_t set_ipvforwarding_routing_table(route_forwarding_info_t* info, const char* address) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    status = routing_ipvforwarding_set_gw_value(info->obj, address);
    when_failed_trace(status, exit, ERROR, "Failed to set the gw parameter");
    status = routing_ipvforwarding_update_routing_table(info, action_add);
    when_failed_trace(status, exit, ERROR, "Failed to update the routing table");
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

static void routing_ipvforwarding_dhcp_defr_ip_changed_cb(UNUSED const char* const sig_name,
                                                          UNUSED const amxc_var_t* const data,
                                                          void* const priv) {
    SAH_TRACEZ_IN(ME);
    route_forwarding_info_t* route_forwarding_info = (route_forwarding_info_t*) priv;
    amxd_status_t status = amxd_status_unknown_error;

    when_null_trace(route_forwarding_info, exit, ERROR, "no route_forwarding_info_t found in the object");
    when_null_trace(data, exit, ERROR, "Invalid data parameter");
    when_null_trace(route_forwarding_info->obj, exit, ERROR, "Not able to find the default route info object");

    when_str_empty(route_forwarding_info->ipv4_route_address, exit);

    status = set_ipvforwarding_routing_table(route_forwarding_info, route_forwarding_info->ipv4_route_address);

exit:
    routing_ipvforwarding_set_status(route_forwarding_info->obj, status == amxd_status_ok ? action_add : action_error);
    SAH_TRACEZ_OUT(ME);
}

static void routing_ipvforwarding_dhcp_ip_changed_cb(UNUSED const char* const sig_name,
                                                     const amxc_var_t* const data,
                                                     void* const priv) {
    SAH_TRACEZ_IN(ME);
    route_forwarding_info_t* route_forwarding_info = (route_forwarding_info_t*) priv;
    amxd_status_t status = amxd_status_unknown_error;

    when_null_trace(route_forwarding_info, exit, ERROR, "no route_forwarding_info_t found in the object");
    when_null_trace(data, exit, ERROR, "Invalid data parameter");
    when_null_trace(route_forwarding_info->obj, exit, ERROR, "Not able to find the default route info object");

    free(route_forwarding_info->ipv4_route_address);
    route_forwarding_info->ipv4_route_address = get_first_string_in_var(data);

    status = set_ipvforwarding_routing_table(route_forwarding_info, route_forwarding_info->ipv4_route_address);
exit:
    routing_ipvforwarding_set_status(route_forwarding_info->obj, status == amxd_status_ok ? action_add : action_error);
    SAH_TRACEZ_OUT(ME);
}

static void routing_ipvforwarding_is_up_changed_cb(UNUSED const char* const sig_name,
                                                   const amxc_var_t* const data,
                                                   void* const priv) {
    SAH_TRACEZ_IN(ME);
    route_forwarding_info_t* route_forwarding_info = (route_forwarding_info_t*) priv;
    amxd_status_t status = amxd_status_unknown_error;
    router_action_t action = action_add;

    when_null_trace(route_forwarding_info, exit, ERROR, "no route_forwarding_info_t found in the object");
    when_null_trace(data, exit, ERROR, "Invalid data parameter");

    route_forwarding_info->is_up = amxc_var_dyncast(bool, data);
    action = route_forwarding_info->is_up ? action_add : action_remove;
    status = routing_ipvforwarding_update_routing_table(route_forwarding_info, action);
    when_failed_trace(status, exit, ERROR, "Failed to update the routing table");
exit:
    routing_ipvforwarding_set_status(route_forwarding_info->obj, status == amxd_status_ok ? action : action_error);
    SAH_TRACEZ_OUT(ME);
}

static void routing_ipvforwarding_name_changed_cb(UNUSED const char* const sig_name,
                                                  const amxc_var_t* const data,
                                                  void* const priv) {
    SAH_TRACEZ_IN(ME);
    route_forwarding_info_t* route_forwarding_info = (route_forwarding_info_t*) priv;
    amxd_status_t status = amxd_status_unknown_error;

    when_null_trace(route_forwarding_info, exit, ERROR, "no route_forwarding_info_t found in the object");
    when_null_trace(data, exit, ERROR, "Invalid data parameter");

    free(route_forwarding_info->intf_name);
    route_forwarding_info->intf_name = get_first_string_in_var(data);
    status = routing_ipvforwarding_update_routing_table(route_forwarding_info, action_add);
    when_failed_trace(status, exit, ERROR, "Failed to update the routing table");

exit:
    routing_ipvforwarding_set_status(route_forwarding_info->obj, status == amxd_status_ok ? action_add : action_error);
    SAH_TRACEZ_OUT(ME);
}

static void routing_ipvforwarding_ip_changed_cb(UNUSED const char* const sig_name,
                                                const amxc_var_t* const data,
                                                void* const priv) {
    SAH_TRACEZ_IN(ME);
    route_forwarding_info_t* route_forwarding_info = (route_forwarding_info_t*) priv;
    amxd_status_t status = amxd_status_unknown_error;

    when_null_trace(route_forwarding_info, exit, ERROR, "no route_forwarding_info_t found in the object");
    when_null_trace(data, exit, ERROR, "Invalid data parameter");

    status = routing_ipvforwarding_set_gw_value(route_forwarding_info->obj, GETP_CHAR(data, ""));
    when_failed_trace(status, exit, ERROR, "Failed to set the gw parameter");
    status = routing_ipvforwarding_update_routing_table(route_forwarding_info, action_add);
    when_failed_trace(status, exit, ERROR, "Failed to update the routing table");
exit:
    routing_ipvforwarding_set_status(route_forwarding_info->obj, status == amxd_status_ok ? action_add : action_error);
    SAH_TRACEZ_OUT(ME);
}

static void routing_ipvforwarding_static_addr_changed_cb(UNUSED const char* const sig_name,
                                                         UNUSED const amxc_var_t* const data,
                                                         void* const priv) {
    SAH_TRACEZ_IN(ME);
    route_forwarding_info_t* route_forwarding_info = (route_forwarding_info_t*) priv;
    amxd_status_t status = amxd_status_unknown_error;

    when_null_trace(route_forwarding_info, exit, ERROR, "no route_forwarding_info_t found in the object");

    free(route_forwarding_info->intf_name);
    route_forwarding_info->intf_name = amxc_var_dyncast(cstring_t, GETP_ARG(data, "0.NetDevName"));

    status = routing_ipvforwarding_update_routing_table(route_forwarding_info, action_add);
    when_failed_trace(status, exit, ERROR, "Failed to update the routing table");
exit:
    routing_ipvforwarding_set_status(route_forwarding_info->obj, status == amxd_status_ok ? action_add : action_error);
    SAH_TRACEZ_OUT(ME);
}

/**
 * Check if the object is an IPv4 forwarding instance.
 * Returns:
 *      - 1 if the object in an IPv4 forwarding instance
 *      - 0 if the object is an IPv6 forwarding instance
 *      - -1 otherwise
 */
static int is_ipv4_forwarding(amxd_object_t* obj) {
    SAH_TRACEZ_IN(ME);
    char* obj_name = NULL;
    int rv = -1;

    when_null_trace(obj, exit, ERROR, "Object is NULL, can't determine if it's and IPv4 forwarding instance");
    obj_name = amxd_object_get_path(obj, AMXD_OBJECT_INDEXED);
    when_str_empty_trace(obj_name, exit, ERROR, "Failed to get name of object, can't determine if it's an IPv4 forwarding instance");

    if(strstr(obj_name, "v4") != NULL) {
        rv = 1;
    } else if(strstr(obj_name, "v6") != NULL) {
        rv = 0;
    }

exit:
    free(obj_name);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief generates a variant containing route information, datamodel data
 * extended with the interface name and the IP version
 * @param info route forwarding info object for which the data should be generated
 * @param data variant where the information should be stored
 */
static void get_variant_for_routing_table(route_forwarding_info_t* info, amxc_var_t* data) {
    SAH_TRACEZ_IN(ME);
    ipvx_t ipvx = IPV4;
    amxd_object_t* parent = NULL;
    const cstring_t parent_name = NULL;

    when_null_trace(data, exit, ERROR, "no valid variant was given");
    when_null_trace(info, exit, ERROR, "no route_forwarding_info_t found in the object");
    when_null_trace(info->obj, exit, ERROR, "route_forwarding_info does not have an obj param");

    amxd_object_get_params(info->obj, data, amxd_dm_access_protected);
    routing_ipvforwarding_remove_unused_param_in_var(data);
    amxc_var_add_key(cstring_t, data, "interface_name", info->intf_name);

    parent = amxd_object_get_parent(info->obj);
    when_null_trace(parent, exit, ERROR, "Could not find the parent");
    parent_name = amxd_object_get_name(amxd_object_get_parent(info->obj), AMXD_OBJECT_NAMED);
    when_null_trace(parent_name, exit, ERROR, "Could not find the parents name");
    if(strncmp(parent_name, "IPv6", strlen("IPv6")) == 0) {
        ipvx = IPV6;
    }
    amxc_var_add_key(uint32_t, data, "IPVx", ipvx);
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static amxd_status_t routing_ipvforwarding_update_routing_table(route_forwarding_info_t* info, router_action_t action) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t data;
    int ret = 0;
    int result = 0;
    bool nothing_changed = false;

    amxc_var_init(&data);

    when_null_trace(info, exit, ERROR, "no route_forwarding_info_t found in the object");
    when_null_trace(info->obj, exit, ERROR, "route_forwarding_info does not have an obj param");

    get_variant_for_routing_table(info, &data);

    when_failed_trace(amxc_var_compare(&data, info->last_set_route, &result), exit, ERROR,
                      "Failed to compare variant");
    nothing_changed = (action == action_add) && (result == 0);
    nothing_changed |= (action == action_remove) && info->last_set_route == NULL;
    when_true_status(nothing_changed, exit, status = amxd_status_ok);

    if(info->last_set_route != NULL) {
        status = routing_manager_call_ctrl(info->obj, ROUTING_ROUTER_ACTION_REMOVE, info->last_set_route);
        amxc_var_delete(&info->last_set_route);
        info->last_set_route = NULL;
    } else {
        status = amxd_status_ok;
    }

    if((action == action_add) && info->is_up) {
        status = routing_manager_call_ctrl(info->obj, ROUTING_ROUTER_ACTION_ADD, &data);
        when_failed(status, exit);

        amxc_var_new(&info->last_set_route);
        ret = amxc_var_copy(info->last_set_route, &data);
        when_false_trace(ret == 0, exit, ERROR, "Failed to copy its var to the last set route");
    }
exit:
    amxc_var_clean(&data);
    SAH_TRACEZ_OUT(ME);
    return status;
}

/**
 * @brief This function will fetch the available information for the forwarding object
 * and store it in the last_set_route part of the info struct linked to this object
 * It will also set the status for this object to "Enabled"
 * If the object already has data stored in the last_set_route data, nothing will be done
 * @param obj IPv4/6Forwarding object which we want to handle
 */
void routing_ipvforwarding_handle_netdev_add(amxd_object_t* const obj) {
    SAH_TRACEZ_IN(ME);
    route_forwarding_info_t* info = NULL;

    when_null_trace(obj, exit, ERROR, "failed to get the obj");
    info = (route_forwarding_info_t*) obj->priv;
    when_null_trace(info, exit, ERROR, "no route_forwarding_info_t found in the object");

    if(info->last_set_route == NULL) {
        amxc_var_new(&info->last_set_route);
        get_variant_for_routing_table(info, info->last_set_route);
        routing_ipvforwarding_set_status(obj, action_add);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void routing_ipvforwarding_handle_netdev_remove(amxd_object_t* const obj) {
    SAH_TRACEZ_IN(ME);
    route_forwarding_info_t* info = NULL;

    when_null_trace(obj, exit, ERROR, "failed to get the obj");
    info = (route_forwarding_info_t*) obj->priv;
    when_null_trace(info, exit, ERROR, "no route_forwarding_info_t found in the object");

    if(info->last_set_route != NULL) {
        amxc_var_delete(&info->last_set_route);
        info->last_set_route = NULL;
        routing_ipvforwarding_set_status(obj, action_error);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

/**
 * @brief Sets the status parameter in the datamodel
 * @param obj The ipv4/6Forwarding object for which the status should be set
 * @param status router action that controls which status will be set
 *               action_add = Enabled, action_remove = Disabled, action_error = Error
 */
static void routing_ipvforwarding_set_status(amxd_object_t* obj, router_action_t status) {
    SAH_TRACEZ_IN(ME);
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    when_null_trace(obj, exit, ERROR, "Object can not be NULL");

    amxd_trans_select_object(&trans, obj);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    if(status == action_add) {
        amxd_trans_set_value(cstring_t, &trans, "Status", "Enabled");
    } else if(status == action_remove) {
        amxd_trans_set_value(cstring_t, &trans, "Status", "Disabled");
    } else if(status == action_error) {
        amxd_trans_set_value(cstring_t, &trans, "Status", "Error");
    } else {
        amxd_trans_set_value(cstring_t, &trans, "Status", "Error");
        SAH_TRACEZ_ERROR(ME, "Invalid status '%d' value is given", status);
    }

    if(amxd_trans_apply(&trans, routing_manager_get_dm()) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to apply the transaction for setting the status");
    }

exit:
    SAH_TRACEZ_OUT(ME);
    amxd_trans_clean(&trans);
}

void routing_ipvforwarding_init(void) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* router_obj = amxd_dm_findf(routing_manager_get_dm(), "Routing.Router.");
    route_forwarding_info_t* info = NULL;
    bool parent_enabled = false;

    amxd_object_iterate(instance, it, router_obj) {
        amxd_object_t* router_inst = amxc_container_of(it, amxd_object_t, it);
        amxd_object_t* ipvforwarding_obj = NULL;
        amxd_object_t* ipvforwarding_inst = NULL;

        parent_enabled = amxd_object_get_value(bool, router_inst, "Enable", NULL);
        routing_ipvforwarding_set_status(router_inst, (parent_enabled ? action_add : action_remove));

        ipvforwarding_obj = amxd_object_get(router_inst, "IPv4Forwarding");
        amxd_object_iterate(instance, it2, ipvforwarding_obj) {
            ipvforwarding_inst = amxc_container_of(it2, amxd_object_t, it);
            info = (route_forwarding_info_t*) ipvforwarding_inst->priv;
            if(info == NULL) {
                when_failed_trace(route_forwarding_info_create(ipvforwarding_inst), exit, ERROR, "Failed to create a route forwarding instance");
                if(parent_enabled && amxd_object_get_value(bool, ipvforwarding_inst, "Enable", NULL)) {
                    routing_ipvforwarding_enable_query(ipvforwarding_inst);
                } else {
                    routing_ipvforwarding_reset_gw_value(ipvforwarding_inst);
                }
            }
        }

        ipvforwarding_obj = amxd_object_get(router_inst, "IPv6Forwarding");
        amxd_object_iterate(instance, it2, ipvforwarding_obj) {
            ipvforwarding_inst = amxc_container_of(it2, amxd_object_t, it);
            info = (route_forwarding_info_t*) ipvforwarding_inst->priv;
            if(info == NULL) {
                when_failed_trace(route_forwarding_info_create(ipvforwarding_inst), exit, ERROR, "Failed to create a route forwarding instance");
                if(parent_enabled && amxd_object_get_value(bool, ipvforwarding_inst, "Enable", NULL)) {
                    routing_ipvforwarding_enable_query(ipvforwarding_inst);
                } else {
                    routing_ipvforwarding_reset_gw_value(ipvforwarding_inst);
                }
            }
        }
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void routing_ipvforwarding_cleanup(void) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* router_obj = amxd_dm_findf(routing_manager_get_dm(), "Routing.Router.");
    amxd_object_iterate(instance, it, router_obj) {
        amxd_object_t* router_inst = amxc_container_of(it, amxd_object_t, it);
        amxd_object_t* ipvforwarding_obj = NULL;
        amxd_object_t* ipvforwarding_inst = NULL;

        ipvforwarding_obj = amxd_object_get(router_inst, "IPv4Forwarding");
        amxd_object_iterate(instance, it2, ipvforwarding_obj) {
            ipvforwarding_inst = amxc_container_of(it2, amxd_object_t, it);
            (void) routing_ipvforwarding_disable_query(ipvforwarding_inst);
            route_forwarding_info_clear(ipvforwarding_inst);
        }

        ipvforwarding_obj = amxd_object_get(router_inst, "IPv6Forwarding");
        amxd_object_iterate(instance, it2, ipvforwarding_obj) {
            ipvforwarding_inst = amxc_container_of(it2, amxd_object_t, it);
            (void) routing_ipvforwarding_disable_query(ipvforwarding_inst);
            route_forwarding_info_clear(ipvforwarding_inst);
        }
    }
    SAH_TRACEZ_OUT(ME);
}

static void update_childs_enable_param(amxd_object_t* router_ipvX_forwarding_obj, bool parent_enabled) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* router_ipvX_forwarding_inst = NULL;
    bool instance_enable = false;
    amxd_status_t status = amxd_status_ok;

    amxd_object_iterate(instance, it, router_ipvX_forwarding_obj) {
        router_ipvX_forwarding_inst = amxc_container_of(it, amxd_object_t, it);
        instance_enable = amxd_object_get_value(bool, router_ipvX_forwarding_inst, "Enable", NULL);
        if(instance_enable) {
            if(parent_enabled == true) {
                routing_ipvforwarding_enable_query(router_ipvX_forwarding_inst);
            } else {
                status = routing_ipvforwarding_disable_query(router_ipvX_forwarding_inst);
                if(status != amxd_status_ok) {
                    SAH_TRACEZ_ERROR(ME, "Failed to disable query for %s", router_ipvX_forwarding_inst->name);
                    routing_ipvforwarding_set_status(router_ipvX_forwarding_inst, action_error);
                } else {
                    routing_ipvforwarding_reset_gw_value(router_ipvX_forwarding_inst);
                }
            }
        }
    }
    SAH_TRACEZ_OUT(ME);
}

static void update_is_static_route_param(amxd_object_t* instance) {
    SAH_TRACEZ_IN(ME);
    bool static_route_value = false;
    cstring_t new_origin_value = NULL;
    bool is_static = false;

    when_null_trace(instance, exit, ERROR, "Failed to find the added instance");
    when_null(amxd_object_get_param_def(instance, "StaticRoute"), exit);

    static_route_value = amxd_object_get_value(bool, instance, "StaticRoute", NULL);
    new_origin_value = amxd_object_get_value(cstring_t, instance, "Origin", NULL);
    when_str_empty_trace(new_origin_value, exit, ERROR, "Invalid new origin value");
    is_static = (strcmp(new_origin_value, "Static") == 0);
    if(is_static != static_route_value) {
        amxd_trans_t trans;
        amxd_trans_init(&trans);

        amxd_trans_select_object(&trans, instance);
        amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
        amxd_trans_set_value(bool, &trans, "StaticRoute", is_static);
        if(amxd_trans_apply(&trans, routing_manager_get_dm()) != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Failed to change the StaticRoute value");
        }
        amxd_trans_clean(&trans);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    free(new_origin_value);
}

static bool routing_ipvforwarding_remove_unused_param_in_var(amxc_var_t* parameters) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t* to_delete = NULL;
    bool changed = 0;

    when_null_trace(parameters, exit, ERROR, "Bad input variable");

    to_delete = amxc_var_take_key(parameters, "Status");
    amxc_var_delete(&to_delete);
    to_delete = amxc_var_take_key(parameters, "StaticRoute");
    amxc_var_delete(&to_delete);

    changed = (amxc_var_get_first(parameters) != NULL);
exit:
    SAH_TRACEZ_OUT(ME);
    return changed;
}

static void ipvforwarding_set_usersetting_flag(amxd_object_t* instance) {
    when_null(instance, exit);
    amxd_object_for_each(parameter, it, instance) {
        amxd_param_t* param = amxc_container_of(it, amxd_param_t, it);
        if(amxd_param_is_attr_set(param, amxd_pattr_persistent)) {
            amxd_param_set_flag(param, "usersetting");
        }
    }
exit:
    return;
}

static void ipvforwarding_clear_usersetting_flag(amxd_object_t* instance) {
    when_null(instance, exit);
    amxd_object_for_each(parameter, it, instance) {
        amxd_param_t* param = amxc_container_of(it, amxd_param_t, it);
        if(amxd_param_has_flag(param, "usersetting")) {
            amxd_param_unset_flag(param, "usersetting");
        }
    }
exit:
    return;
}

void _routing_ipvforwarding_global_enable_changed(UNUSED const char* const sig_name,
                                                  const amxc_var_t* const data,
                                                  UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* router_inst = NULL;
    bool enable = false;

    router_inst = amxd_dm_signal_get_object(routing_manager_get_dm(), data);
    when_null_trace(router_inst, exit, ERROR, "Failed to get router instance");

    enable = amxd_object_get_value(bool, router_inst, "Enable", NULL);
    update_childs_enable_param(amxd_object_get(router_inst, "IPv4Forwarding"), enable);
    update_childs_enable_param(amxd_object_get(router_inst, "IPv6Forwarding"), enable);
    routing_ipvforwarding_set_status(router_inst, (enable ? action_add : action_remove));
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _routing_ipvforwarding_added(UNUSED const char* const sig_name,
                                  const amxc_var_t* const data,
                                  UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* obj = amxd_dm_signal_get_object(routing_manager_get_dm(), data);
    amxd_object_t* instance = amxd_object_get_instance(obj, NULL, GET_UINT32(data, "index"));
    bool is_enabled = false;
    bool parent_enabled = false;

    when_null_trace(instance, exit, ERROR, "Failed to find the added instance");
    if(instance->priv == NULL) {
        when_failed_trace(route_forwarding_info_create(instance), exit, ERROR, "Failed to create a route forwarding instance");

        parent_enabled = amxd_object_get_value(bool, amxd_object_get_parent(amxd_object_get_parent(instance)), "Enable", NULL);
        is_enabled = amxd_object_get_value(bool, instance, "Enable", NULL);
        if(is_enabled && parent_enabled) {
            routing_ipvforwarding_enable_query(instance);
        } else {
            routing_ipvforwarding_reset_gw_value(instance);
        }
    }
    if((GETP_CHAR(data, "parameters.Origin") != NULL) && (strcmp(GETP_CHAR(data, "parameters.Origin"), "Static") != 0)) {
        ipvforwarding_clear_usersetting_flag(instance);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _routing_ipvforwarding_changed(UNUSED const char* const sig_name,
                                    const amxc_var_t* const data,
                                    UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* instance = amxd_dm_signal_get_object(routing_manager_get_dm(), data);
    bool is_enabled = false;
    bool parent_enabled = false;
    route_forwarding_info_t* info = NULL;
    amxc_var_t* params = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    bool valid_event = false;

    when_null_trace(instance, exit, ERROR, "Failed to find the changed instance");

    info = (route_forwarding_info_t*) instance->priv;
    when_null_trace(info, exit, ERROR, "no route_forwarding_info_t found in the object");

    params = GET_ARG(data, "parameters");
    amxc_var_for_each(param, params) {
        const char* key = amxc_var_key(param);
        if((key != NULL) && (strcmp(key, "Status") != 0) && (strcmp(key, "StaticRoute") != 0)) {
            valid_event = true;
            break;
        }
    }
    // Ignore event if only Status and/or StaticRoute changed
    when_false(valid_event, exit);

    if(info->added_by_netdev) {
        routing_ipvforwarding_set_status(instance, action_error);
        SAH_TRACEZ_ERROR(ME, "Cannot change IPvXForwarding instances added by netdev");
        goto exit;
    }

    if(GETP_ARG(params, "Origin.to") != NULL) {
        strcmp(GETP_CHAR(params, "Origin.to"), "Static") == 0 ? ipvforwarding_set_usersetting_flag(instance) : ipvforwarding_clear_usersetting_flag(instance);
    }

    parent_enabled = amxd_object_get_value(bool, amxd_object_get_parent(amxd_object_get_parent(instance)), "Enable", NULL);
    is_enabled = amxd_object_get_value(bool, instance, "Enable", NULL);
    if((GET_ARG(params, "Interface") != NULL) || (GET_ARG(params, "Enable") != NULL) || (GET_ARG(params, "Origin") != NULL)) {
        routing_ipvforwarding_disable_query(instance);
        if(is_enabled && parent_enabled) {
            routing_ipvforwarding_enable_query(instance);
        } else {
            routing_ipvforwarding_reset_gw_value(instance);
        }
    } else if(is_enabled && parent_enabled) {
        status = routing_ipvforwarding_update_routing_table(info, action_add);
        routing_ipvforwarding_set_status(instance, status == amxd_status_ok ? action_add : action_error);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}
