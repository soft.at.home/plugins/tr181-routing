/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "route_event_manager.h"
#include "netdev_interface_name.h"
#include "routing_ipvforwarding_info.h"
#include "routing_ipvforwarding.h"
#include "common.h"

#include <amxc/amxc.h>
#include <amxd/amxd_object.h>
#include <debug/sahtrace.h>

#include <stdlib.h>
#include <routing_manager.h>
#include <string.h>
#include <stdio.h>

static void delete_table_entry(UNUSED const char* key, amxc_htable_it_t* hit);
static rmi_return_code_t route_lut_add_item(amxc_htable_t* lut, const char* key, amxd_object_t* object);
static amxc_string_t* get_netdev_object_path(const amxc_var_t* const netdev_data);

rmi_return_code_t route_event_manager_add_forwarding_instance(route_event_manager_t* self,
                                                              const char* base_object,
                                                              const char* netdev_path,
                                                              const char* table_name,
                                                              amxc_var_t* forwarding_instance_data,
                                                              amxd_object_t** ret_object) {
    SAH_TRACEZ_IN(ME);
    rmi_return_code_t return_value = rmi_error;
    amxd_object_t* table_obj = amxd_dm_findf(routing_manager_get_dm(), "Routing.Router.[RoutingTableName=='%s'].", table_name);

    when_str_empty_trace(table_name, exit, ERROR, "No table name provided");
    when_null_trace(table_obj, exit, WARNING, "No table found named '%s'", table_name);

    if(amxc_htable_get(self->route_lut, netdev_path)) {
        SAH_TRACEZ_INFO(ME, "Not adding new instance: duplicate key [%s]", netdev_path);
        return_value = rmi_e_duplicate;
        goto exit;
    }
    *ret_object = self->finder(self->route_lut, forwarding_instance_data);
    if(NULL != *ret_object) {
        SAH_TRACEZ_INFO(ME, "Route instance found. Link with netdev path %s", netdev_path);
        routing_ipvforwarding_handle_netdev_add(*ret_object);
    } else {
        amxd_status_t rc = dm_interface_add_ip_forwarding_instance(base_object, forwarding_instance_data, ret_object);
        when_failed_trace(rc, exit, ERROR, "Failed to add Forwarding instance to Router");
    }
    return_value = route_lut_add_item(self->route_lut, netdev_path, *ret_object);

exit:
    SAH_TRACEZ_OUT(ME);
    return return_value;
}

rmi_return_code_t route_event_manager_netdev_route_event_add(route_event_manager_t* self,
                                                             const char* base_object,
                                                             const amxc_var_t* const event_data,
                                                             amxc_var_t* forwarding_instance_data,
                                                             amxd_object_t** ret_object) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t* netdev_path = NULL;
    rmi_return_code_t return_value = rmi_error;

    netdev_path = get_netdev_object_path(event_data);
    when_null(netdev_path, exit);

    return_value = route_event_manager_add_forwarding_instance(self,
                                                               base_object,
                                                               amxc_string_get(netdev_path, 0),
                                                               GETP_CHAR(event_data, "parameters.Table"),
                                                               forwarding_instance_data, ret_object);

exit:
    amxc_string_delete(&netdev_path);
    SAH_TRACEZ_OUT(ME);
    return return_value;
}

rmi_return_code_t route_event_manager_netdev_route_event_remove(route_event_manager_t* self,
                                                                const amxc_var_t* const data) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t* netdev_path = NULL;
    rmi_return_code_t rc = rmi_error;
    amxc_htable_it_t* it = NULL;
    const char* netdev_path_str = NULL;
    table_entry_t* entry = NULL;
    netdev_path = get_netdev_object_path(data);
    route_forwarding_info_t* info = NULL;

    when_null(netdev_path, exit);
    netdev_path_str = amxc_string_get(netdev_path, 0);
    when_null(netdev_path_str, exit);

    it = amxc_htable_take(self->route_lut, netdev_path_str);
    when_null_trace(it, exit, WARNING, "Could not find IP Forwarding for NetDev object [%s]", netdev_path_str);

    entry = amxc_container_of(it, table_entry_t, it);
    when_null(entry, exit);
    if(NULL != entry->instance) {
        info = (route_forwarding_info_t*) entry->instance->priv;

        if((info != NULL) && (info->added_by_netdev == false)) {
            routing_ipvforwarding_handle_netdev_remove(entry->instance);
        } else {
            dm_interface_remove_ip_forwarding_instance(&entry->instance);
            when_true((entry->instance != NULL), exit);
        }
    }
    rc = rmi_ok;
exit:
    amxc_htable_it_clean(it, delete_table_entry);
    amxc_string_delete(&netdev_path);
    SAH_TRACEZ_OUT(ME);
    return rc;
}

rmi_return_code_t route_event_manager_init(route_event_manager_t* self) {
    SAH_TRACEZ_IN(ME);
    amxc_htable_t* route_lut = NULL;
    rmi_return_code_t rc = rmi_error;

    when_null(self, exit);
    when_failed(amxc_htable_new(&route_lut, 32), exit);

    self->route_lut = route_lut;
    rc = rmi_ok;

exit:
    SAH_TRACEZ_OUT(ME);
    return rc;
}

void route_event_manager_cleanup(route_event_manager_t* self) {
    SAH_TRACEZ_IN(ME);
    if(NULL != self) {
        amxc_htable_delete(&(self->route_lut), delete_table_entry);
    }
    SAH_TRACEZ_OUT(ME);
}

rmi_return_code_t route_event_manager_remove_object(route_event_manager_t* self, amxd_object_t* object) {
    SAH_TRACEZ_IN(ME);
    rmi_return_code_t rc = rmi_error;

    when_null(self, exit);
    when_null(object, exit);

    rc = rmi_ok;
    amxc_htable_for_each(iter, self->route_lut) {
        table_entry_t* entry = amxc_container_of(iter, table_entry_t, it);

        if((NULL != entry) && (object == entry->instance)) {
            entry->instance = NULL;
            break;
        }
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return rc;
}

static void delete_table_entry(UNUSED const char* key, amxc_htable_it_t* hit) {
    SAH_TRACEZ_IN(ME);
    if(NULL != hit) {
        table_entry_t* entry = amxc_container_of(hit, table_entry_t, it);
        free(entry);
    }
    SAH_TRACEZ_OUT(ME);
}

static rmi_return_code_t route_lut_add_item(amxc_htable_t* lut, const char* key, amxd_object_t* object) {
    SAH_TRACEZ_IN(ME);
    rmi_return_code_t rv = rmi_error;
    table_entry_t* entry = (table_entry_t*) calloc(1, sizeof(table_entry_t));

    when_null(lut, exit);
    when_null(key, exit);
    when_null(entry, exit);
    entry->instance = object;

    when_failed(amxc_htable_insert(lut, key, &entry->it), exit);
    rv = rmi_ok;

exit:
    if(rv != rmi_ok) {
        free(entry);
    }
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static amxc_string_t* get_netdev_object_path(const amxc_var_t* const netdev_data) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t* path = NULL;
    uint32_t index = GETP_UINT32(netdev_data, "index");
    const char* netdev_path = GETP_CHAR(netdev_data, "path");

    when_null(netdev_data, exit);
    when_str_empty(netdev_path, exit);

    amxc_string_new(&path, 0);
    amxc_string_setf(path, "%s%u.", netdev_path, index);

exit:
    SAH_TRACEZ_OUT(ME);
    return path;
}
