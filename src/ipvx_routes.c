/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "common.h"
#include "ipvx_routes.h"
#include "routing_manager.h"
#include "routing_ipvforwarding.h"
#include "routing_ipvforwarding_info.h"
#include "netdev_interface_name.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

static rmi_return_code_t ipvx_routes_handle_initial_routes(const amxc_htable_t* routes,
                                                           route_event_manager_t* mgr,
                                                           const ipvx_routes_config_t* config);
static rmi_return_code_t ipvx_routes_add_netdev_route(amxc_var_t* netdev_route,
                                                      const char* key,
                                                      route_event_manager_t* mgr,
                                                      const ipvx_routes_config_t* config);
static amxd_status_t netdev_parameters_transform(const amxc_var_t* const netdev_params,
                                                 amxc_var_t* forwarding_parameters,
                                                 const ipvx_routes_config_t* config);

rmi_return_code_t ipvx_routes_initial_route_population(route_event_manager_t* route_event_manager,
                                                       const ipvx_routes_config_t* config) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t query;
    const amxc_llist_t* query_items = NULL;
    rmi_return_code_t rc = rmi_ok;

    amxc_var_init(&query);
    when_null(route_event_manager, exit);
    when_null(config, exit);

    when_false((AMXB_STATUS_OK == amxb_get(routing_manager_get_netdev_context(), config->netdev_query, 0, &query, 10)), exit);

    query_items = amxc_var_constcast(amxc_llist_t, &query);
    when_null(query_items, exit);

    amxc_llist_for_each(query_item, query_items) {
        amxc_var_t* routes_var = amxc_llist_it_get_data(query_item, amxc_var_t, lit);
        const amxc_htable_t* routes = NULL;
        if(NULL == routes_var) {
            continue;
        }
        routes = amxc_var_constcast(amxc_htable_t, routes_var);
        when_false((rc = ipvx_routes_handle_initial_routes(routes, route_event_manager, config)) == rmi_ok, exit)
    }

exit:
    amxc_var_clean(&query);
    SAH_TRACEZ_OUT(ME);
    return rc;
}

bool ipvx_routes_table_contains_object(amxc_htable_t* lut, amxd_object_t* obj) {
    SAH_TRACEZ_IN(ME);
    bool found_obj = false;

    amxc_htable_for_each(iter, lut) {
        table_entry_t* entry = amxc_container_of(iter, table_entry_t, it);

        if((NULL != entry) && (obj == entry->instance)) {
            found_obj = true;
            break;
        }
    }

    SAH_TRACEZ_OUT(ME);
    return found_obj;
}

rmi_return_code_t ipvx_routes_add_route(route_event_manager_t* route_event_manager,
                                        const amxc_var_t* const event_data,
                                        const ipvx_routes_config_t* config) {
    SAH_TRACEZ_IN(ME);
    rmi_return_code_t rc = rmi_error;
    amxc_var_t* netdev_parameters = GET_ARG(event_data, "parameters");
    amxc_var_t ipvx_forwarding;
    amxd_object_t* object = NULL;

    amxc_var_init(&ipvx_forwarding);

    when_null_trace(route_event_manager, exit, ERROR, "No route event manager provided");
    when_null_trace(config, exit, ERROR, "No route config provided");
    when_null_trace(netdev_parameters, exit, ERROR, "parameters not available in NetDev data");
    when_failed_trace(netdev_parameters_transform(netdev_parameters, &ipvx_forwarding, config), exit, ERROR, "Failed to transform data");

    amxc_var_add_key(cstring_t, &ipvx_forwarding, "Status", "Enabled");
    amxc_var_add_key(bool, &ipvx_forwarding, "Enable", true);
    amxc_var_add_key(cstring_t, &ipvx_forwarding, "Origin", "Automatic");

    rc = route_event_manager_netdev_route_event_add(route_event_manager,
                                                    config->object_template,
                                                    event_data,
                                                    &ipvx_forwarding,
                                                    &object);
    when_failed_trace(rc, exit, ERROR, "Failed to create forwarding instance");
    if((object->priv != NULL) && ((route_forwarding_info_t*) object->priv)->added_by_netdev) {
        rc = netdev_set_interface_name(GETP_CHAR(event_data, "path"), object);
    }
exit:
    amxc_var_clean(&ipvx_forwarding);
    SAH_TRACEZ_OUT(ME);
    return rc;
}

amxd_status_t copy_parameter(int param,
                             const amxc_var_t* const netdev_params,
                             amxc_var_t* forwarding_params,
                             const ipvx_routes_config_t* config) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rc = amxd_status_unknown_error;
    amxc_var_t* item = NULL;

    when_null(netdev_params, exit);
    when_null(forwarding_params, exit);
    when_null(config, exit);
    when_null(config->to_str, exit);

    item = GET_ARG(netdev_params, config->mapping[param].netdev_param);
    when_null(item, exit);

    if(0 == amxc_var_set_key(forwarding_params,
                             config->to_str(param),
                             item,
                             AMXC_VAR_FLAG_COPY)) {
        rc = amxd_status_ok;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rc;
}

static rmi_return_code_t ipvx_routes_handle_initial_routes(const amxc_htable_t* routes,
                                                           route_event_manager_t* mgr,
                                                           const ipvx_routes_config_t* config) {
    SAH_TRACEZ_IN(ME);
    rmi_return_code_t rc = rmi_ok;

    when_null(routes, exit);

    amxc_htable_for_each(route_it, routes) {
        amxc_var_t* route = amxc_htable_it_get_data(route_it, amxc_var_t, hit);
        if(NULL == route) {
            continue;
        }
        when_false(((rc = ipvx_routes_add_netdev_route(route, amxc_htable_it_get_key(route_it), mgr, config)) == rmi_ok), exit);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return rc;
}

static rmi_return_code_t ipvx_routes_add_netdev_route(amxc_var_t* netdev_route,
                                                      const char* key,
                                                      route_event_manager_t* mgr,
                                                      const ipvx_routes_config_t* config) {
    SAH_TRACEZ_IN(ME);
    rmi_return_code_t rc = rmi_ok;
    amxc_var_t route;
    amxd_object_t* object = NULL;

    amxc_var_init(&route);

    when_null(netdev_route, exit);
    when_null(mgr, exit);
    when_failed(netdev_parameters_transform(netdev_route, &route, config), exit);

    amxc_var_add_key(cstring_t, &route, "Status", "Enabled");
    amxc_var_add_key(bool, &route, "Enable", true);
    amxc_var_add_key(cstring_t, &route, "Origin", "Automatic");
    rc = route_event_manager_add_forwarding_instance(mgr,
                                                     config->object_template,
                                                     key,
                                                     GETP_CHAR(netdev_route, "Table"),
                                                     &route,
                                                     &object);
    when_false_trace(rc == rmi_ok, exit, ERROR, "Failed to create forwarding instance");
    if((object->priv != NULL) && ((route_forwarding_info_t*) object->priv)->added_by_netdev) {
        rc = netdev_set_interface_name(key, object);
    }
exit:
    amxc_var_clean(&route);
    SAH_TRACEZ_OUT(ME);
    return rc;
}

static amxd_status_t netdev_parameters_transform(const amxc_var_t* const netdev_params,
                                                 amxc_var_t* forwarding_parameters,
                                                 const ipvx_routes_config_t* config) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rc = amxd_status_unknown_error;

    when_null_trace(netdev_params, exit, ERROR, "No variant provided");
    when_null_trace(forwarding_parameters, exit, ERROR, "No forwarding variant provided");
    when_null_trace(config, exit, ERROR, "No route config provided");
    when_failed_trace(amxc_var_set_type(forwarding_parameters, AMXC_VAR_ID_HTABLE), exit, ERROR, "Failed to set variant type");

    for(uint32_t item = 0; item < config->size; ++item) {
        if(NULL != config->mapping[item].transform) {
            rc = config->mapping[item].transform(item, netdev_params, forwarding_parameters, config);
            when_failed(rc, exit);
        }
    }

    if(NULL != config->tr181_compl_transform) {
        rc = config->tr181_compl_transform(forwarding_parameters);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rc;
}
