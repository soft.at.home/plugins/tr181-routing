/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "netdev_integration.h"

typedef struct _subscription_data {
    const char* object_path;
    const char* expression;
    amxp_slot_fn_t cb_fn;
} subscription_data_t;

static void callback_netdev_link_ipv4_route_added_event(const char* const sig_name,
                                                        const amxc_var_t* const data,
                                                        void* const priv);
static void callback_netdev_link_ipv4_route_removed_event(const char* const sig_name,
                                                          const amxc_var_t* const data,
                                                          void* const priv);
static void callback_netdev_link_ipv6_route_added_event(const char* const sig_name,
                                                        const amxc_var_t* const data,
                                                        void* const priv);
static void callback_netdev_link_ipv6_route_removed_event(const char* const sig_name,
                                                          const amxc_var_t* const data,
                                                          void* const priv);

static const subscription_data_t netdev_subscription[] = {
    {
        "NetDev.Link.",
        "notification == 'dm:instance-added' &&"
        "path matches 'NetDev\\.Link\\.[0-9]+\\.IPv4Route\\.'",
        callback_netdev_link_ipv4_route_added_event
    },
    {
        "NetDev.Link.",
        "notification == 'dm:instance-removed' &&"
        "path matches 'NetDev\\.Link\\.[0-9]+\\.IPv4Route\\.'",
        callback_netdev_link_ipv4_route_removed_event
    },
    {
        "NetDev.Link.",
        "notification == 'dm:instance-added' &&"
        "path matches 'NetDev\\.Link\\.[0-9]+\\.IPv6Route\\.'",
        callback_netdev_link_ipv6_route_added_event
    },
    {
        "NetDev.Link.",
        "notification == 'dm:instance-removed' &&"
        "path matches 'NetDev\\.Link\\.[0-9]+\\.IPv6Route\\.'",
        callback_netdev_link_ipv6_route_removed_event
    },
};

rmi_return_code_t netdev_subscribe_events(routing_manager_t* rm) {
    SAH_TRACEZ_IN(ME);
    rmi_return_code_t rc = rmi_error;
    amxb_subscription_t* subscription = NULL;

    when_null(rm, exit);
    for(int i = 0; i < (int) (sizeof(netdev_subscription) / sizeof(netdev_subscription[0])); ++i) {
        int rv = amxb_subscription_new(&subscription,
                                       routing_manager_get_netdev_context(),
                                       netdev_subscription[i].object_path,
                                       netdev_subscription[i].expression,
                                       netdev_subscription[i].cb_fn, rm);
        when_failed_trace(rv, exit, ERROR,
                          "Cannot subscribe for NetDev event: [path=%s,expression=%s]",
                          netdev_subscription[i].object_path,
                          netdev_subscription[i].expression);

        when_failed(subscription_manager_add(&rm->subscription_manager, subscription), exit);
    }
    rc = rmi_ok;
exit:
    SAH_TRACEZ_OUT(ME);
    return rc;
}

static void callback_netdev_link_ipv4_route_added_event(UNUSED const char* const sig_name,
                                                        const amxc_var_t* const data,
                                                        void* const priv) {
    SAH_TRACEZ_IN(ME);
    routing_manager_t* manager = (routing_manager_t*) priv;

    when_null(manager, exit);

    SAH_TRACEZ_INFO(ME, "NetDev IPv4 Route added");
    if(rmi_ok != ipv4_routes_add_route(&manager->ipv4_routes, data)) {
        SAH_TRACEZ_ERROR(ME, "NetDev IPv4 Route added event callback failed");
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void callback_netdev_link_ipv4_route_removed_event(UNUSED const char* const sig_name,
                                                          const amxc_var_t* const data,
                                                          void* const priv) {
    SAH_TRACEZ_IN(ME);
    routing_manager_t* manager = (routing_manager_t*) priv;

    when_null(manager, exit);

    SAH_TRACEZ_INFO(ME, "NetDev IPv4 Route remove");
    if(rmi_ok != ipv4_routes_remove_route(&manager->ipv4_routes, data)) {
        SAH_TRACEZ_ERROR(ME, "NetDev IPv4 Route removed event callback failed");
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void callback_netdev_link_ipv6_route_added_event(UNUSED const char* const sig_name,
                                                        const amxc_var_t* const data,
                                                        void* const priv) {
    SAH_TRACEZ_IN(ME);
    routing_manager_t* manager = (routing_manager_t*) priv;

    when_null(manager, exit);

    SAH_TRACEZ_INFO(ME, "NetDev IPv6 Route add");
    if(rmi_ok != ipv6_routes_add_route(&manager->ipv6_routes, data)) {
        SAH_TRACEZ_ERROR(ME, "NetDev IPv6 Route add event callback failed");
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void callback_netdev_link_ipv6_route_removed_event(UNUSED const char* const sig_name,
                                                          const amxc_var_t* const data,
                                                          void* const priv) {
    SAH_TRACEZ_IN(ME);
    routing_manager_t* manager = (routing_manager_t*) priv;

    when_null(manager, exit);

    SAH_TRACEZ_INFO(ME, "NetDev IPv6 Route remove");
    if(rmi_ok != ipv6_routes_remove_route(&manager->ipv6_routes, data)) {
        SAH_TRACEZ_ERROR(ME, "NetDev IPv6 Route remove event callback failed");
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}
