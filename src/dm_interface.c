/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "common.h"
#include "dm_interface.h"
#include "routing_manager.h"
#include "routing_ipvforwarding_info.h"

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_action.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>

#include <stdlib.h>

amxd_status_t dm_interface_add_ip_forwarding_instance(const char* base_object,
                                                      amxc_var_t* ipvx_route_data,
                                                      amxd_object_t** instance) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rc = amxd_status_unknown_error;
    amxd_object_t* root = NULL;
    route_forwarding_info_t* info = NULL;

    when_null(base_object, exit);

    root = amxd_dm_findf(routing_manager_get_dm(), "%s", base_object);
    when_null(root, exit);

    rc = amxd_object_add_instance(instance, root, NULL, 0, ipvx_route_data);
    when_false(amxd_status_ok == rc, exit)
    amxd_object_set_attr(*instance, amxd_oattr_persistent, false);

    amxd_object_for_each(parameter, it, *instance) {
        amxd_param_t* param = amxc_container_of(it, amxd_param_t, it);
        amxd_param_set_attr(param, amxd_pattr_read_only, true);
    }

    route_forwarding_info_create(*instance);
    when_null(*instance, exit);
    info = (route_forwarding_info_t*) (*instance)->priv;
    when_null_trace(info, exit, ERROR, "Private data should never be NULL, netdev otherwise would not change this instance");
    info->added_by_netdev = true;

    amxd_object_emit_add_inst(*instance);
exit:
    SAH_TRACEZ_OUT(ME);
    return rc;
}

void dm_interface_remove_ip_forwarding_instance(amxd_object_t** instance) {
    SAH_TRACEZ_IN(ME);
    route_forwarding_info_t* info = NULL;

    when_null(instance, exit);
    when_null(*instance, exit);
    info = (route_forwarding_info_t*) (*instance)->priv;
    when_null_trace(info, exit, ERROR, "Private data should never be NULL, netdev otherwise would not change this instance");

    info->removed_by_netdev = true;

    amxd_object_emit_del_inst(*instance);
    amxd_object_delete(instance);
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}
