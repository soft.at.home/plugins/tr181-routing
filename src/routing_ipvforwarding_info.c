/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "routing_ipvforwarding_info.h"
#include "routing_manager.h"

#include <stdlib.h>
#include <stdio.h>

/**********************************************************
* Type definitions
**********************************************************/

/**********************************************************
* Variable declarations
**********************************************************/

/**********************************************************
* Function Prototypes
**********************************************************/

/**********************************************************
* Functions
**********************************************************/

amxd_status_t route_forwarding_info_create(amxd_object_t* obj) {
    SAH_TRACEZ_IN(ME);
    route_forwarding_info_t* route_forwarding_info = NULL;
    amxd_status_t status = amxd_status_unknown_error;

    when_null_trace(obj, exit, ERROR, "invalid obj given");
    when_false_trace(obj->priv == NULL, exit, ERROR, "Stop creating a route forwarding obj, private data is not NULL");

    route_forwarding_info = (route_forwarding_info_t*) calloc(1, sizeof(route_forwarding_info_t));
    when_null_trace(route_forwarding_info, exit, ERROR, "Failed to calloc a default_route_t object");

    obj->priv = route_forwarding_info;
    route_forwarding_info->obj = obj;
    route_forwarding_info->intf_path = NULL;
    route_forwarding_info->intf_name = NULL;
    route_forwarding_info->last_set_route = NULL;
    route_forwarding_info->nm_query = NULL;
    route_forwarding_info->nm_query2 = NULL;
    route_forwarding_info->nm_query_is_up = NULL;
    route_forwarding_info->nm_query_ipv4_addr = NULL;
    route_forwarding_info->added_by_netdev = false;
    route_forwarding_info->removed_by_netdev = false;
    route_forwarding_info->is_up = true;

    status = amxd_status_ok;
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

void route_forwarding_info_clear(amxd_object_t* obj) {
    SAH_TRACEZ_IN(ME);
    route_forwarding_info_t* info = NULL;
    when_null_trace(obj, exit, ERROR, "no valid object given");
    info = (route_forwarding_info_t*) obj->priv;
    when_null_trace(info, exit, WARNING, "no valid route_forwarding_info_t given");

    route_forwarding_info_clear_content(info);
    info->obj = NULL;
    free(info);
    obj->priv = NULL;
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void route_forwarding_info_clear_content(route_forwarding_info_t* info) {
    SAH_TRACEZ_IN(ME);
    when_null_trace(info, exit, ERROR, "no valid route_forwarding_info_t given");
    if(info->nm_query != NULL) {
        netmodel_closeQuery(info->nm_query);
        info->nm_query = NULL;
    }
    if(info->nm_query2 != NULL) {
        netmodel_closeQuery(info->nm_query2);
        info->nm_query2 = NULL;
    }
    if(info->nm_query_is_up != NULL) {
        netmodel_closeQuery(info->nm_query_is_up);
        info->nm_query_is_up = NULL;
    }
    if(info->nm_query_ipv4_addr != NULL) {
        netmodel_closeQuery(info->nm_query_ipv4_addr);
        info->nm_query_ipv4_addr = NULL;
    }

    amxc_var_delete(&info->last_set_route);
    info->last_set_route = NULL;

    free(info->intf_name);
    info->intf_name = NULL;

    free(info->intf_path);
    info->intf_path = NULL;

    free(info->ipv4_route_address);
    info->ipv4_route_address = NULL;
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}
