/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "common.h"
#include "ipv4_routes.h"
#include "routing_manager.h"
#include "ipvx_routes.h"
#include "routing_ipvforwarding_info.h"

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxc/amxc_variant.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_expression.h>

#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define STR_NOT_EMPTY(x) (x != NULL && *x != 0)

typedef enum {
    Forwarding_DestIPAddress,
    Forwarding_DestSubnetMask,
    Forwarding_GatewayIPAddress,
    Forwarding_ForwardingMetric,
    Forwarding_Nr_
} forwarding_param_t;

static const char* forwarding_param_str[Forwarding_Nr_] = {
    "DestIPAddress",
    "DestSubnetMask",
    "GatewayIPAddress",
    "ForwardingMetric"
};

static amxd_status_t dest_subnet_mask_param(int param,
                                            const amxc_var_t* const netdev_params,
                                            amxc_var_t* forwarding_params,
                                            const ipvx_routes_config_t* config);
static amxd_status_t tr181_mapper(amxc_var_t* forwarding_params);
static const char* forwarding_param_to_str(int param);
static rmi_return_code_t bitlength_to_subnetmask(char* buffer, size_t size, uint8_t bit_length);
static amxd_object_t* find_ipv4_route(amxc_htable_t* lut, amxc_var_t* params);

static netdev_transform_t mapping[Forwarding_Nr_] = {
    [Forwarding_DestIPAddress] = {"Dst", copy_parameter},
    [Forwarding_DestSubnetMask] = {"DstLen", dest_subnet_mask_param},
    [Forwarding_GatewayIPAddress] = {"Gateway", copy_parameter},
    [Forwarding_ForwardingMetric] = {"Priority", copy_parameter}
};

static const ipvx_routes_config_t ipv4_config = {
    .netdev_query = "NetDev.Link.*.IPv4Route.*.",
    .object_template = "Routing.Router.main.IPv4Forwarding.",
    .to_str = forwarding_param_to_str,
    .mapping = mapping,
    .tr181_compl_transform = tr181_mapper,
    .size = Forwarding_Nr_
};

static inline void tr181_map(const char* item, amxc_var_t* const forwarding_params) {
    SAH_TRACEZ_IN(ME);
    const char* parameter = GETP_CHAR(forwarding_params, item);

    SAH_TRACEZ_INFO(ME, "Parameter %s Key %s", parameter, item);
    if((NULL != parameter) && (0 == strcmp(parameter, "0.0.0.0"))) {
        amxc_var_t param;
        amxc_var_init(&param);
        amxc_var_set_cstring_t(&param, "");
        SAH_TRACEZ_INFO(ME, "Replace parameter %s with %s for Key %s",
                        parameter, amxc_var_constcast(cstring_t, &param), item);
        amxc_var_set_key(forwarding_params, item, &param, AMXC_VAR_FLAG_COPY);
        amxc_var_clean(&param);
    }
    SAH_TRACEZ_OUT(ME);
}
rmi_return_code_t ipv4_routes_add_route(ipv4_routes_t* self,
                                        const amxc_var_t* const data) {
    SAH_TRACEZ_IN(ME);
    rmi_return_code_t rv = ipvx_routes_add_route(&self->route_event_manager, data, &ipv4_config);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

rmi_return_code_t ipv4_routes_remove_route(ipv4_routes_t* self,
                                           const amxc_var_t* const data) {
    SAH_TRACEZ_IN(ME);
    rmi_return_code_t rv = route_event_manager_netdev_route_event_remove(&(self->route_event_manager), data);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

rmi_return_code_t ipv4_routes_initial_route_population(ipv4_routes_t* self) {
    SAH_TRACEZ_IN(ME);
    rmi_return_code_t rv = rmi_error;

    when_null(self, exit);
    rv = ipvx_routes_initial_route_population(&self->route_event_manager, &ipv4_config);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

rmi_return_code_t ipv4_routes_init(ipv4_routes_t* self) {
    SAH_TRACEZ_IN(ME);
    rmi_return_code_t rc = rmi_error;

    when_null(self, exit);
    rc = route_event_manager_init(&self->route_event_manager);
    when_failed_trace(rc, exit, ERROR, "Failed to init ipv4_routes object");

    self->route_event_manager.finder = find_ipv4_route;
exit:
    SAH_TRACEZ_OUT(ME);
    return rc;
}

void ipv4_routes_cleanup(ipv4_routes_t* self) {
    SAH_TRACEZ_IN(ME);
    if(NULL != self) {
        route_event_manager_cleanup(&self->route_event_manager);
    }
    SAH_TRACEZ_OUT(ME);
}

static const char* forwarding_param_to_str(int param) {
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_OUT(ME);
    return ((Forwarding_DestIPAddress <= param) && (Forwarding_Nr_ > param))
           ? forwarding_param_str[param] : NULL;
}

static amxd_status_t dest_subnet_mask_param(int param,
                                            const amxc_var_t* const netdev_params,
                                            amxc_var_t* forwarding_params,
                                            UNUSED const ipvx_routes_config_t* config) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rc = amxd_status_unknown_error;
    amxc_var_t* item = NULL;
    char subnetmask[16] = { 0 };

    when_null(netdev_params, exit);
    when_null(forwarding_params, exit);

    item = GETP_ARG(netdev_params, mapping[param].netdev_param);
    when_null(item, exit);

    when_false((0 == bitlength_to_subnetmask(subnetmask, sizeof(subnetmask), amxc_var_dyncast(uint8_t, item))),
               exit);

    if(NULL != amxc_var_add_key(cstring_t,
                                forwarding_params,
                                forwarding_param_to_str(param),
                                subnetmask)) {
        rc = amxd_status_ok;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rc;
}

static rmi_return_code_t bitlength_to_subnetmask(char* buffer, size_t size, uint8_t bit_length) {
    SAH_TRACEZ_IN(ME);
    rmi_return_code_t rv = rmi_error;
    const uint8_t num_zeroes = 32 - bit_length;
    const uint32_t mask = 0xFFFFFFFF << num_zeroes;
    const char* mask_substr = NULL;
    uint8_t mask_byte = 0;
    int return_value = 0;

    const size_t num_bytes = 4;
    for(int idx = num_bytes - 1; idx >= 0; --idx) {
        mask_byte = (mask >> (idx * 8)) & 0xFF;
        mask_substr = (idx == 0) ? "%u" : "%u.";
        return_value = snprintf(buffer, size, mask_substr, mask_byte);
        if((return_value < 0) || ((size_t) return_value > size)) {
            goto exit;
        }
        buffer += return_value;

        if(size < (size_t) return_value) {
            goto exit;
        }
        size -= return_value;
    }

    rv = rmi_ok;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static amxd_object_t* find_ipv4_route(amxc_htable_t* lut, amxc_var_t* params) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* route = NULL;
    amxd_object_t* instances = amxd_dm_findf(routing_manager_get_dm(), "Routing.Router.main.IPv4Forwarding.");
    amxp_expr_t expression;
    amxc_string_t filter;
    const char* dest_ip = GETP_CHAR(params, "DestIPAddress");
    const char* dest_mask = GETP_CHAR(params, "DestSubnetMask");
    const char* gw_ip = GETP_CHAR(params, "GatewayIPAddress");
    bool has_gw_ip = false;
    bool default_route = false;

    amxc_string_init(&filter, 0);

    when_null(params, exit);
    when_null(instances, exit);

    amxc_string_setf(&filter,
                     "DestIPAddress==\"%s\" && DestSubnetMask==\"%s\"",
                     dest_ip, dest_mask);

    if(STR_NOT_EMPTY(dest_ip) && STR_NOT_EMPTY(dest_mask) &&
       ((strcmp(dest_ip, "0.0.0.0") == 0) && (strcmp(dest_mask, "0.0.0.0") == 0))) {
        default_route = true;
    }

    if(STR_NOT_EMPTY(gw_ip)) {
        has_gw_ip = true;
        amxc_string_appendf(&filter, " && GatewayIPAddress==\"%s\"", gw_ip);
    }

    amxp_expr_init(&expression, amxc_string_get(&filter, 0));
    route = amxd_object_find_instance(instances, &expression);

    SAH_TRACEZ_INFO(ME, "DestIPAddress:%s/DestSubnetMask:%s", dest_ip, dest_mask);
    SAH_TRACEZ_INFO(ME, "This is %s default route, gateway IP is %s empty", default_route ? "" : "not", has_gw_ip ? "not" : "");

    while(route != NULL) {
        char* origin = amxd_object_get_value(cstring_t, route, "Origin", NULL);
        if(STR_NOT_EMPTY(origin)) {
            if(!default_route || has_gw_ip || (strcmp(origin, "IPCP") == 0) || (strcmp(origin, "3GPP-NAS") == 0)) {
                if(ipvx_routes_table_contains_object(lut, route) == false) {
                    free(origin);
                    break;
                }
            }
        }

        route = amxd_object_find_next_instance(route, &expression);
        free(origin);
    }
    amxp_expr_clean(&expression);
exit:
    amxc_string_clean(&filter);
    SAH_TRACEZ_OUT(ME);
    return route;
}

static amxd_status_t tr181_mapper(amxc_var_t* forwarding_params) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rc = amxd_status_unknown_error;

    when_null(forwarding_params, exit);

    tr181_map("DestIPAddress", forwarding_params);
    tr181_map("GatewayIPAddress", forwarding_params);

    rc = amxd_status_ok;

exit:
    SAH_TRACEZ_OUT(ME);
    return rc;
}
