{% let has_upstream = (BDfn.getUpstreamInterfaceIndex() != "-1") %}
include "global_ip_interfaces.odl";

%populate {
    object 'Routing.Router' {
        instance add ("main"){
            parameter Alias = "main";
            parameter Enable = true;
            parameter RoutingTableName = "main";
        }
    }
    object 'Routing.RouteInformation' {
        parameter Enable = true;
    }
    object 'Routing.RouteInformation.InterfaceSetting' {
        instance add ("main"){
            {% if (has_upstream): %}
            parameter Interface = "${ip_intf_wan}";
            {% else %}
            parameter Interface = "${ip_intf_lan}";
            {% endif %}
        }
    }
    object 'Routing.Router.main.IPv4Forwarding' {
        instance add ("cpe-default") {
            parameter Alias = "cpe-default";
            parameter DestIPAddress = "0.0.0.0";
            parameter DestSubnetMask = "0.0.0.0";
            parameter Enable = true;
            {% if (has_upstream): %}
            parameter Interface = "${ip_intf_wan}";
            {% else %}
            parameter Interface = "${ip_intf_lan}";
            {% endif %}
            parameter Origin = "DHCPv4";
        }
    }
    {% if (has_upstream): %}
    object 'Routing.Router.main.IPv6Forwarding' {
        instance add ("prefix_blackhole-default") {
            parameter Alias = "prefix_blackhole-default";
            parameter Type = "Blackhole";
            parameter Enable = false;
            parameter Interface = "${ip_intf_lo}";
            parameter DestIPPrefix = "";
            parameter Origin = "Static";
        }
    }
    {% endif %}
}
