#include "mod_sahtrace.odl";
#include "mod_apparmor.odl";
#include "global_amxb_timeouts.odl";

%config {
    %global ip_intf_lo = "Device.IP.Interface.1.";
    %global ip_intf_wan = "Device.IP.Interface.2.";
    %global ip_intf_lan = "Device.IP.Interface.3.";
}

#include "global_ip_interfaces.odl";

%config {
    dm-eventing-enabled = true;
    name = "routing-manager";
    storage-path = "${rw_data_path}/${name}";
    prefix_ = "X_PRPL-COM_";

    dm-events-before-start = true;
    odl = {
        load-dm-events = true,
        dm-save = true,
        dm-save-on-changed = true,
        dm-save-delay = 1000,
        directory = "${storage-path}/odl"
    };

    // SAHTRACE
    sahtrace = {
        type = "syslog",
        level = "${default_log_level}"
    };
    trace-zones = { "routing" = "${default_trace_zone_level}",
                    "mod-routing" = "${default_trace_zone_level}",
                    "mod-routing-dhcp" = "${default_trace_zone_level}"};

    definition_file = "${name}-definition.odl";
    defaults_file = "${name}-defaults.odl";
    save_file = "${rw_data_path}/${name}/${name}/${name}.odl";

    include_dirs = [
        "/etc/amx/modules/",
        "."
    ];

    import-dirs = [
        ".",
        "${prefix}${plugin-dir}/${name}",
        "${prefix}${plugin-dir}/modules",
        "${prefix}/usr/lib/amx/${name}",
        "${prefix}/usr/lib/amx/modules"
    ];

    NetModel = "nm_iprouter";
    nm_iprouter = {
        InstancePath = "Routing.RouteInformation.InterfaceSetting.",
        Tags = "iprouter",
        InterfaceReference = "Interface"
    };

    pcm_svc_config = {
       	"Objects" = "Routing",
        "BackupFiles" = "routing-manager"
    };
}

import "${name}.so" as "${name}";
import "mod-dmext.so";
import "mod-netmodel.so" as "mod_nm";

#include "routing-manager_caps.odl";

include "${definition_file}";
#include "mod-routing-dhcp.odl";
?include "${odl.directory}":"${defaults_file}";

requires "NetModel.Intf.";
requires "NetDev.";

%define {
    entry-point mod_nm.mod_netmodel_main;
    entry-point routing-manager.routing_manager_main;
}
#include "mod_pcm_svc.odl";
