/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_route_information.h"
#include "routing_route_information.h"
#include "routing_manager.h"
#include "routing_route_information_netmodel.h"
#include "test_utils.h"

#include <stdlib.h>
#include <setjmp.h>
#include <cmocka.h>
#include <string.h>
#include <netinet/in.h>

#include "../mocks/mock_amxo.h"
#include "../mocks/mock_netmodel.h"
#include "../mocks/mock_socket.h"

static amxo_fd_read_t start_stop_socket(intf_info_t* intf_info, bool open_socket) {
    amxp_slot_fn_t callback;
    amxc_var_t ret_var;
    amxo_fd_read_t write_data_socket;

    amxc_var_init(&ret_var);

    callback = get_netmodel_openQuery_isUp_callback();
    assert_non_null(callback);

    //open socket
    if(open_socket) {
        amxc_var_set(cstring_t, &ret_var, "eth0");
    } else {
        amxc_var_set(cstring_t, &ret_var, "");
    }

    callback(NULL, &ret_var, (void*) intf_info);
    write_data_socket = get_write_data_socket_cb();

    amxc_var_clean(&ret_var);

    return write_data_socket;
}

static intf_info_t* get_intf_info(cstring_t path) {
    intf_info_t* info = NULL;
    amxd_object_t* interface_setting = NULL;

    interface_setting = amxd_dm_findf(test_get_dm(), path);
    _route_information_enable_changed(NULL, NULL, NULL);
    assert_non_null(interface_setting);
    assert_non_null(interface_setting->priv);

    info = (intf_info_t*) interface_setting->priv;

    return info;
}

static void add_interface(cstring_t intf_name) {
    amxd_trans_t trans;
    amxd_dm_t* dm = NULL;
    amxd_object_t* route_information = NULL;
    amxd_status_t status = amxd_status_unknown_error;

    dm = test_get_dm();
    route_information = amxd_dm_findf(dm, "Routing.RouteInformation.InterfaceSetting");
    assert_non_null(route_information);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, route_information);
    amxd_trans_add_inst(&trans, 0, NULL);

    amxd_trans_set_value(cstring_t, &trans, "Interface", intf_name);
    status = amxd_trans_apply(&trans, dm);
    assert_true(status == amxd_status_ok);

    amxd_trans_clean(&trans);
}

void test_route_information_start_multiple_intf(UNUSED void** state) {
    amxd_dm_t* dm = NULL;
    amxd_object_t* interface_setting_1 = NULL;
    amxd_object_t* interface_setting_2 = NULL;
    intf_info_t* info = NULL;

    dm = test_get_dm();
    routing_route_information_cleanup();

    interface_setting_1 = amxd_dm_findf(dm, "Routing.RouteInformation.InterfaceSetting.1");//default interface that is added
    assert_non_null(interface_setting_1);
    assert_null(interface_setting_1->priv);

    add_interface("Device.IP.Interface.3.");
    interface_setting_2 = amxd_dm_findf(dm, "Routing.RouteInformation.InterfaceSetting.[Interface == 'Device.IP.Interface.3.']");
    assert_non_null(interface_setting_2);
    assert_null(interface_setting_2->priv);

    _route_information_enable_changed(NULL, NULL, NULL);
    assert_non_null(interface_setting_1->priv);
    assert_non_null(interface_setting_2->priv);

    info = (intf_info_t*) interface_setting_2->priv;
    assert_true(strncmp(info->intf_path, "Device.IP.Interface.3.", 13) == 0);
    assert_null(info->intf_name);
    assert_true(info->obj = interface_setting_2);
    assert_true(info->socket_fd == -1);

    routing_route_information_cleanup();
    assert_null(interface_setting_1->priv);
    assert_null(interface_setting_2->priv);
}

void test_start_stop_socket(UNUSED void** state) {
    amxo_fd_read_t write_data_socket;
    cstring_t status;

    _route_information_enable_changed(NULL, NULL, NULL);
    intf_info_t* info = get_intf_info("Routing.RouteInformation.InterfaceSetting.1");

    //open socket
    write_data_socket = start_stop_socket(info, 1);
    assert_true(strcmp(info->intf_name, "eth0") == 0);
    assert_non_null(write_data_socket);
    assert_true(info->socket_fd != -1);
    status = amxd_object_get_value(cstring_t, info->obj, "Status", NULL);
    assert_string_equal(status, "NoForwardingEntry");
    free(status);

    //close socket
    write_data_socket = start_stop_socket(info, 0);
    assert_null(info->intf_name);
    assert_true(info->socket_fd == -1);
    status = amxd_object_get_value(cstring_t, info->obj, "Status", NULL);
    assert_string_equal(status, "NoForwardingEntry");
    free(status);
}

void test_write_to_socket(UNUSED void** state) {
    amxo_fd_read_t write_data_socket;
    char* src_router = "ff00ff00ff00ff00"; // = 6666:3030:6666:3030:6666:3030:6666:3030
    uint8_t subnet = 32;
    char* seconds = "8888";                // = (hex)38383838 = (int)943208504
    char* prefix = "11111111";             // = (hex)3131:3131:3131:3131
    amxc_ts_t* result_time = NULL;
    char* result_src_router = NULL;
    char* result_prefix = NULL;
    cstring_t status = NULL;

    _route_information_enable_changed(NULL, NULL, NULL);
    intf_info_t* info = get_intf_info("Routing.RouteInformation.InterfaceSetting.1");

    //open socket
    write_data_socket = start_stop_socket(info, 1);

    //Send data
    set_src_router_to_send(src_router);
    set_data_to_send(subnet, seconds, prefix);
    write_data_socket(info->socket_fd, (void*) info);

    //check result
    result_src_router = amxd_object_get_value(cstring_t, info->obj, "SourceRouter", NULL);
    result_time = amxd_object_get_value(amxc_ts_t, info->obj, "RouteLifetime", NULL);
    result_prefix = amxd_object_get_value(cstring_t, info->obj, "Prefix", NULL);
    status = amxd_object_get_value(cstring_t, info->obj, "Status", NULL);
    assert_string_equal(status, "ForwardingEntryCreated");
    assert_true(strcmp(result_src_router, "6666:3030:6666:3030:6666:3030:6666:3030") == 0);
    assert_true(strcmp(result_prefix, "3131:3131:3131:3131::/32") == 0);
    assert_non_null(result_time);

    free(result_src_router);
    free(result_prefix);
    free(result_time);
    free(status);
    //close socket
    write_data_socket = start_stop_socket(info, 0);
}

void test_write_multiple_times_to_socket(UNUSED void** state) {
    amxo_fd_read_t write_data_socket;
    char* src_router = "ff00ff00ff00ff00"; // = 6666:3030:6666:3030:6666:3030:6666:3030
    uint8_t subnet = 32;
    char* seconds = "8888";                // = (hex)38383838 = (int)943208504
    char* prefix = "11111111";             // = (hex)3131:3131:3131:3131
    amxc_ts_t* result_time = NULL;
    char* result_src_router = NULL;
    char* result_prefix = NULL;
    cstring_t status = NULL;

    _route_information_enable_changed(NULL, NULL, NULL);
    intf_info_t* info = get_intf_info("Routing.RouteInformation.InterfaceSetting.1");

    //open socket
    write_data_socket = start_stop_socket(info, 1);

    //Send data
    set_src_router_to_send(src_router);
    set_data_to_send(subnet, seconds, prefix);
    write_data_socket(info->socket_fd, (void*) info);

    //Send data
    set_src_router_to_send(src_router);
    set_data_to_send(subnet, seconds, prefix);
    write_data_socket(info->socket_fd, (void*) info);

    //Send data
    set_src_router_to_send(src_router);
    set_data_to_send(subnet, seconds, prefix);
    write_data_socket(info->socket_fd, (void*) info);

    //check result
    result_src_router = amxd_object_get_value(cstring_t, info->obj, "SourceRouter", NULL);
    result_time = amxd_object_get_value(amxc_ts_t, info->obj, "RouteLifetime", NULL);
    result_prefix = amxd_object_get_value(cstring_t, info->obj, "Prefix", NULL);
    status = amxd_object_get_value(cstring_t, info->obj, "Status", NULL);
    assert_string_equal(status, "ForwardingEntryCreated");
    assert_true(strcmp(result_src_router, "6666:3030:6666:3030:6666:3030:6666:3030") == 0);
    assert_true(strcmp(result_prefix, "3131:3131:3131:3131::/32") == 0);
    assert_non_null(result_time);

    free(result_src_router);
    free(result_prefix);
    free(result_time);
    free(status);
    //close socket
    write_data_socket = start_stop_socket(info, 0);
}
