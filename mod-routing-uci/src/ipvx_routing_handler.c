/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <stdio.h>
#include <netinet/in.h>
#include <net/route.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <net/if.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <errno.h>

#include "ipvx_route_handler.h"
#include "ipvx.h"
#include "uci_ctrl.h"

#define ME "mod-routing"

static bool ipv4_route_add(const amxc_var_t* data);
static bool ipv6_route_add(const amxc_var_t* data);
static bool ipvx_route_uci_remove(const amxc_var_t* data);

bool ipvx_route_add(const amxc_var_t* const data) {
    ipvx_t version = IPV4;
    bool rc = false;

    when_null(data, exit);
    version = GETP_UINT32(data, "IPVx");

    switch(version) {
    case IPV4: {
        rc = ipv4_route_add(data);
    }
    break;
    case IPV6: {
        rc = ipv6_route_add(data);
    } break;
    default:
        break;
    }

exit:
    return rc;
}

bool ipvx_route_remove(const amxc_var_t* const data) {
    return ipvx_route_uci_remove(data);
}

static bool ipv4_route_add(const amxc_var_t* data) {
    bool rc = false;
    amxc_var_t parameters;
    amxc_var_init(&parameters);
    amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &parameters, "interface", GETP_CHAR(data, "Interface"));
    amxc_var_add_key(cstring_t, &parameters, "gateway", GETP_CHAR(data, "GatewayIPAddress"));
    amxc_var_add_key(cstring_t, &parameters, "target", GETP_CHAR(data, "DestIPAddress"));
    amxc_var_add_key(cstring_t, &parameters, "netmask", GETP_CHAR(data, "DestSubnetMask"));

    when_failed(uci_call("add", "network", GETP_CHAR(data, "Alias"), "route", &parameters, NULL), exit);
    rc = (0 == uci_call("commit", "network", GETP_CHAR(data, "Alias"), "route", NULL, NULL));

exit:
    amxc_var_clean(&parameters);
    return rc;
}
static bool ipv6_route_add(const amxc_var_t* data) {
    bool rc = false;
    amxc_var_t parameters;
    amxc_var_init(&parameters);
    amxc_var_set_type(&parameters, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &parameters, "interface", GETP_CHAR(data, "Interface"));
    amxc_var_add_key(cstring_t, &parameters, "target", GETP_CHAR(data, "DestIPPrefix"));

    when_failed(uci_call("add", "network", GETP_CHAR(data, "Alias"), "route6", &parameters, NULL), exit);
    rc = (0 == uci_call("commit", "network", GETP_CHAR(data, "Alias"), "route6", NULL, NULL));

exit:
    amxc_var_clean(&parameters);
    return rc;
}
static bool ipvx_route_uci_remove(const amxc_var_t* data) {
    ipvx_t version = IPV4;
    bool rc = false;
    amxc_var_t ret;

    amxc_var_init(&ret);
    when_null(data, exit);
    version = GETP_UINT32(data, "IPVx");

    when_failed(uci_call("get", "network", GETP_CHAR(data, "Alias"), NULL, NULL, &ret), exit);
    amxc_var_t* route = GETP_ARG(&ret, "0.values");

    if(NULL == route) {
        rc = true;
        goto exit;
    }

    when_failed(uci_call(
                    "delete",
                    "network",
                    GETP_CHAR(data, "Alias"),
                    (IPV4 == version ? "route" : "route6"),
                    NULL,
                    NULL),
                exit);

    rc = ( 0 == uci_call("commit",
                         "network",
                         GETP_CHAR(data, "Alias"),
                         (IPV4 == version ? "route" : "route6"),
                         NULL,
                         NULL));
exit:
    amxc_var_clean(&ret);
    return rc;
}
